<define-tag pagetitle>Meilleure couverture vidéo pendant DebConf, grâce à nos parrains !</define-tag>
<define-tag release_date>2011-07-26</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="1.1" maintainer="David Prévot"

<p>
Le projet Debian a le plaisir d'annoncer que, grâce aux dons de plusieurs
parrains, les flux vidéos en direct des conférences Debian sont
maintenant beaucoup plus facilement fournis aux personnes intéressées.

<q>Alors que les conférences Debian précédentes, la couverture vidéo ne
reposait que sur la bonne volonté individuelle, celle-ci sera la
première avec une retransmission intégrale de toutes les sessions !</q>
déclare Holger Levsen, chef de l'équipe vidéo de DebConf : <q>Les
vidéos seront en plus de meilleure qualité que celles habituellement
proposées avec le matériel qui était loué les années précédentes</q>.

Cela est rendu possible grâces aux dons de <a
href="http://www.hp.com/go/linux">HP</a> et de l'<a
href="http://www.irill.org/">IRILL</a> : le premier a fourni
plusieurs HP elitebook 2540s tandis que le second a donné environ
15 000 € avec lesquels l'équipe vidéo de DebConf a pu acheter deux
caméras professionnelles avec trépieds et deux autres caméras.

De plus, l'IRILL fournira un espace de stockage
sécurisé pour l'équipement quand il n'est pas utilisé.
</p>

<p>
Les flux vidéos et les enregistrements seront tous deux tous deux disponibles
pour toutes les présentations de DebConf11 et les BoF : le programme de la
conférence et les liens pertinents sont disponibles sur la <a
href="http://debconf11.debconf.org/watch.xhtml">page dédiée à DebConf</a>.

Le projet Debian tient à remercier HP et l'IRILL pour leur généreux
parrainage et l'équipe vidéo de DebConf pour leur fabuleux travail.

Vous pouvez laisser un mot de remerciement à l'équipe vidéo de DebConf sur la
<a href="http://wiki.debconf.org/action/edit/DebConf11/Videoteam/Thanks">page
de remerciement</a>.
</p>


<h2>À propos de HP</h2>

<p>
HP est une entreprise informatique multinationale localisée aux
États-Unis et fondée en 1939 qui propose des solutions libres et sous
Linux pour tous types d'entreprise, des plus grandes aux plus petites.
</p>

<h2>À propos de l'IRILL</h2>

<p>
l'IRILL (Initiative de Recherche et Innovation sur le Logiciel Libre) a été
créée par l'UPMC, l'Université Paris Diderot et l'INRIA pour s'occuper de
défis significatifs venant de la généralisation du logiciel libre,
rassemblant au même endroit des chercheurs et des scientifiques à la pointe,
des développeurs experts du libre et des acteurs industriels du libre.
</p>

 
<h2>À propos de Debian</h2>

<p>
Le projet Debian a été fondé en 1993 par Ian Murdock
pour être un projet communautaire réellement libre.
Depuis cette date, le projet Debian est devenu l'un des plus
importants et des plus influents projets à code source ouvert.
Des milliers de volontaires du monde entier travaillent
ensemble pour créer et maintenir les logiciels Debian.
Disponible en soixante-dix langues et prenant en charge un
grand nombre de types d'ordinateurs, la distribution Debian est
conçue pour être le <q>système d'exploitation universel</q>.
</p>


<h2>À propos de DebConf</h2>

<p>
DebConf est la conférence des développeurs du projet Debian.

En plus d'un programme complet de présentations techniques,
sociales ou organisationnelles, DebConf fournit aux développeurs,
aux contributeurs et à toutes personnes intéressées, une occasion
de rencontre et de travail collaboratif interactif.

DebConf a eu lieu depuis 2000 en des endroits du monde
aussi variés que le Canada, la Finlande ou le Mexique.

Plus de renseignements sont disponibles sur le <a
href="http://debconf.org/">site de DebConf</a>.
</p>


<h2>À propos de DebConf11</h2>

<p>
Plus de renseignements sur DebConf11 sont disponibles sur le <a
href="http://debconf11.debconf.org/">site de la conférence</a> ou en contactant
l'équipe générale de presse de la Debconf en &lt;press@debconf.org&gt;.
</p>


<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site
Internet de Debian <a href="$(HOME)/">http://www.debian.org/</a>
ou envoyez un courrier électronique à &lt;press@debian.org&gt;.
</p>

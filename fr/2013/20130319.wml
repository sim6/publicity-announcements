
<define-tag pagetitle>Debian dans les nuages</define-tag>

<define-tag release_date>2013-03-19</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="1.1" maintainer="Cédric Boutillier"

<p>
Comme vous le savez peut-être, à partir de la prochaine version stable
<q>Wheezy</q>, Debian <a 
href="http://www.debian.org/News/2012/20120425">facilitera à ses
utilisateurs le déploiement de nuages privés</a>.
En tant que projet de logiciel libre, nous faisons attention à la
liberté de nos utilisateurs, et nous leur recommandons de faire tourner
leur propre nuage dès que possible.
</p>

<p>
Bien que nous vous recommandions d'avoir votre propre nuage, nous
comprenons qu'un nuage privé n'est pas une option pour de nombreux
utilisateurs de Debian, qui, pour toutes sortes de raisons, ont besoin
de compter sur des offres de nuage public.
Et c'est en pensant à ces utilisateurs que nous avons décidé d'offrir
des images Debian sur des nuages publics.
Pour le moment, ces images sont disponibles pour deux fournisseurs
populaires :
<a href="...">Amazon EC2</a> et
<a href="...">Microsoft Azure</a>.
</p>

<p>
Le projet Debian ne soutient aucun nuage public. Nous voudrions plutôt, avec
l'aide de notre communauté, aider les utilisateurs
potentiels de Debian sur une grande variété de fournisseurs, dès lors
que les images seront totalement libres et gratuites, et pourront être
créées avec des outils entièrement libres.
Si vous souhaitez aider, ou simplement en savoir plus sur cet effort,
inscrivez-vous à la <a href="http://lists.debian.org/debian-cloud">liste
de diffusion debian-cloud</a> et contribuez à ses activités.
Tout bogue lié aux images pour nuage doit être signalé contre le
<a href="http://bugs.debian.org/cloud.debian.org">pseudo-paquet
<tt>cloud.debian.org</tt></a>.
</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis, le projet a grandi pour devenir
l'un des plus gros et plus influents projets à code source ouvert. Des
milliers de volontaires du monde entier travaillent ensemble pour
créer et maintenir les logiciels Debian. Traduite dans plus de
soixante-dix langues et gérant un grand nombre de types d'ordinateurs,
Debian se nomme elle-même le <q>système d'exploitation universel</q>.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site
Internet de Debian <a href="$(HOME)/">http://www.debian.org/</a>
ou envoyez un courrier électronique à &lt;press@debian.org&gt;.
</p>

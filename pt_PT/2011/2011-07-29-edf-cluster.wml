# Status: [sent]
# $Id$
# $Rev$

<define-tag pagetitle>Novo cluster de cálculo beowulf na EDF (200 Tflops) baseado em Debian 6.0 Squeeze</define-tag>
<define-tag release_date>2011-07-29</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="1.1"

<p>A Électricité de France S.A. tem o prazer de anunciar que o seu novo supercomputador,
com 200 Tflops e 43º do último <a
href="http://www.top500.org/system/10804">TOP500 (Junho 2011)</a>, é baseado em 
Debian Squeeze.</p>

<p>Este supercomputador, chamado Ivanoe, e constituído por nós de computadores, 
nós de tratamento gráfico, servidores de conexão e servidores de infraestrutura. 
Isto representa 1454 servidores IDataPlex IBM e 200 Tflops.</p>

<p>Os nós de computadores, de tratamento gráfico e servidores de conexão não 
possuem disco rígido e são totalmente autónomos graças ao <a 
href="$(HOME)CD/live/"><q>Debian Live Project</q></a>, no entanto existem alguns 
servidores que têm instalação em disco graças à <a href="http://fai-project.org/">
<q>Fully Automatic Installation (FAI)</q></a> que é configurada através do <a 
href="http://projects.puppetlabs.com/projects/puppet">Puppet</q></a>.</p>

<p>Para concretizar esta instalação as equipas de engenheiros da EDF tiveram que 
integrar componentes de software proprietárias em ambiente Debian, mais precisamente 
o Sistema de Ficheiros General Parallel (GPFS) da IBM e o controlador Infiniband 
QLogic usado pela rede Infiniband.</p>

<p>A compilação da imagem de sistema foi baseada no repositório oficial Debian e é 
composta por 900 pacotes. Os pacotes instalados combinam com os requisitos para 
o software de cálculo da EDF usado pela companhia.</p>

<p>As equipas de engenheiros envolvidos neste projecto gostariam de agradecer ao 
Stefano Zacchiroli pela sua ajuda e disponibilidade, a todos os Debian
Developers pela qualidade das suas contribuições à comunidade, e finalmente à IBM, 
a construtora, por ajudar no suporte a Debian no seu hardware.</p>

<p>A EDF escolheu esta estratégia de homogeneização para a sua computação 
científica baseada num conjunto comum de software, chamado Debian. Uma equipa 
interna de engenheiros está encarregue de desenvolver e adaptar a distribuição de 
modo a responder a necessidades especificas da computação científica. Debian na 
EDF representa 1050 estações de trabalho dedicadas ao uso científico e 9 
clusters beowulf que representam 2132 nós de computadores.</p>


<h2>Sobre o grupo EDF</h2>

<p>O grupo EDF, é um dos líderes Europeus no mercado da energia, é um fornecedor 
integrado de energia com operação em sectores variados, entre os quais se inclui 
geração de energia, transporte, distribuição, mercado e vendas.  O grupo é líder 
na produção de energia na Europa. Entre o transporte EDF e distribuição por 
subsidiários opera 1,285,000 km de linhas aéreas e subterrâneas de baixa e média 
tensão e cerca de 100,000 km em rede de alta e muito alta tensão. O grupo 
está envolvido no fornecimento de energia e serviços a mais de 28 milhões de 
consumidores em França.</p>


<h2>Sobre Debian</h2>

<p>O Projecto Debian foi fundado em 1993 por Ian Murdock para ser um projecto 
de comunidade verdadeiramente livre. Desde então o projecto cresceu e é hoje um 
dos maiores e mais influentes projectos de código fonte livre. Milhares de 
voluntários de todos o mundo trabalham em conjunto para criar e manter o software 
Debian. Disponível em 70 idiomas e abrangendo uma ampla variedade de tipos de 
computadores, a Debian intitula-se o <q>sistema operativo universal</q>.</p>


<h2>Contactos</h2>

<p>Para mais informações, por favor visite as páginas web da Debian em 
<a href="$(HOME)/">http://www.debian.org/</a> ou envie mail para 
&lt;press@debian.org&gt;.</p>


